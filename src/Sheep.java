import java.util.Arrays;

public class Sheep {

	   enum Animal {sheep, goat};
	   
	   public static void main (String[] param) {
	      Animal [] animalList = { Animal.sheep, Animal.goat, Animal.sheep, Animal.sheep, Animal.goat};
	      reorder(animalList);
	      System.out.println(Arrays.toString(animalList));
	      
	   }
	   
	   public static void reorder (Animal[] animals) {
		  //esimene loom
	      int first = 0;
	      //viimane loom
	      int last = animals.length - 1;
	      //Kuna tuleb välja et Arrays.sort on liiga aeglane n log(n), siis oleks vaja kasutada mõnda kiiremat algoritmi
	      //hakkame massiivi mõlemast otsast kontrollima ja vahetame elementide kohad, kus vaja. Üsna sarnane quicksort meetodile.
	      while(first < last){
	    	  if (animals[first] == Animal.goat){
	    		  first = first + 1;
	    	  }
	    	  else if (animals[last] == Animal.sheep){
	    		  last = last - 1;
	    	  }
	    	  else {
	    		  Animal a = animals[first];
	    		  animals[first] = animals[last];
	    		  animals[last] = a;
	    	  }
	    		  
	   }
	}
}